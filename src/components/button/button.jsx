import { Component } from "react";
import "./button.scss";

export default class Button extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <button
          onClick={this.props.onClick}
          id={this.props.data}
          className={this.props.backgroundColor}
        >
    
          {this.props.text}
        </button>
      </>
    );
  }
}
