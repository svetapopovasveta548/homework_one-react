import { Component } from "react";
import "./modal.scss";
import Button from "../button/button";

export default class Modal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <div className="modal" onClick={this.props.onClick}>
          <div
            className={
              this.props.id === "modalID1"
                ? "modal__wrapper-one"
                : "modal__wrapper-two"
            }
            onClick={(e) => e.stopPropagation()}
          >
            <div className="header-wrapper">
              <p
                className={
                  this.props.id === "modalID1" ? "header-one" : "header-two"
                }
              >
                {this.props.header && this.props.header}
              </p>


              {this.props.closeButton && this.props.id == "modalID1" && <Button text='x' backgroundColor='header-wrapper__cross' onClick={this.props.onClickCross}/>}
  
            </div>

            <div
              className={this.props.id === "modalID1" ? "desc-one" : "desc-two"}
            >
              {this.props.text && this.props.text}
            </div>
            <div className="button-wraper">
              {this.props.actions.lentgh != 0 && this.props.actions.map(action => (
                <Button backgroundColor={action.backgroundColor} text={action.text}/>
              ))}

            </div>
          </div>
        </div>
      </>
    );
  }
}
