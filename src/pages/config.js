const modalWindowDeclarations = [
  {
    id: "modalID1",
    title: "Title for modal 1",
    description: "Lorem ipsum dolor sit amet consectetur, adipisicing elit?",
  },
  {
    id: "modalID2",
    title: "Title for modal 2",
    description: "Amet consectetur adipisicing elit. Tempore, earum?",
  },
];

export default modalWindowDeclarations;
