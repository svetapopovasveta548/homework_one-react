import { Component } from "react";
import Button from "../components/button/button";
import Modal from "../components/modal/modal";
import modalWindowDeclarations from "./config.js";

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      id: null,
      title: null,
      description: null,
      closeButton: true,
      actions: [
        { backgroundColor: "btn-one__first", text: "Click1" },
        { backgroundColor: "btn-two__second", text: "Click2" },
      ],
      // actions: [],

    };
  }

    // componentDidMount() {
    //   // console.log(item);
    //   fetch("./mocup.json")
    //     .then((res) => res.json())
    //     .then((data) => {
    //      this.setState({actions: data})
    //      console.log(this.state.actions)
  
    //     }
    //     )
    //   };

  onClickButton = (e) => {
    const modalID = e.target.id; // то по какой кнопке был клик
    const modalDeclaration = modalWindowDeclarations.find(
      (item) => item.id === modalID
    );
    const { title, description, id } = modalDeclaration;
    this.setState((prev) => ({
      ...prev,
      isOpen: true,
      title: title,
      description: description,
      id: id,
    }));
  };

  onClose = () => {
    this.setState(() => ({ isOpen: false }));
  };

  render() {
    return (
      <div className="wrapper-button">
        <Button
          onClick={(e) => {
            this.onClickButton(e);
          }}
          text="Open first modal"
          backgroundColor="main-button__one"
          data="modalID1"
        ></Button>
        <Button
          text="Open second modal"
          backgroundColor="main-button__two"
          data="modalID2"
          onClick={(e) => {
            this.onClickButton(e);
          }}
        ></Button>
        {this.state.isOpen && (
          <Modal
            onClick={(e) => this.onClose()}
            id={this.state.id}
            header={this.state.title}
            text={this.state.description}
            onClickCross={(e) => this.onClose()}
            actions={this.state.actions}
            closeButton={this.state.closeButton}
          />
        )}
      </div>
    );
  }
}
